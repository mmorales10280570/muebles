/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carro;

import entidad.Mueble;
import java.util.*;

/**
 *
 * @author ARMAND
 */
public class Carro 
{
    List<CarroItem> items;
    int numeroDeItems;
    double total;

    public Carro() {
        items = new ArrayList<CarroItem>();
        numeroDeItems = 0;
        total = 0;
    }

    /**
     * Adds a <code>CarroItem</code> to the <code>Carro</code>'s
     * <code>items</code> list. If item of the specified <code>mueble</code>
     * already exists in shopping carro list, the cantidad of that item is
     * incremented.
     *
     * @param mueble the <code>Mueble</code> that defines the type of shopping carro item
     * @see CarroItem
     */
    public synchronized void agregaItem(Mueble mueble) {

        boolean newItem = true;

        for (CarroItem scItem : items) {

            if (scItem.getMueble().getIdMueble() == mueble.getIdMueble()) {

                newItem = false;
                scItem.incrementaCantidad();
            }
        }

        if (newItem) {
            CarroItem scItem = new CarroItem(mueble);
            items.add(scItem);
        }
    }

    /**
     * Updates the <code>CarroItem</code> of the specified
     * <code>mueble</code> to the specified cantidad. If '<code>0</code>'
     * is the given cantidad, the <code>CarroItem</code> is removed
     * from the <code>Carro</code>'s <code>items</code> list.
     *
     * @param mueble the <code>Mueble</code> that defines the type of shopping carro item
     * @param cantidad the number which the <code>CarroItem</code> is updated to
     * @see CarroItem
     */
    public synchronized void actualiza(Mueble mueble, String cantidad) {

        short cant = -1;

        // cast cantidad as short
        cant = Short.parseShort(cantidad);

        if (cant >= 0) {

            CarroItem item = null;

            for (CarroItem scItem : items) {

                if (scItem.getMueble().getIdMueble() == mueble.getIdMueble()) {

                    if (cant != 0) {
                        // set item cantidad to new value
                        scItem.setCantidad(cant);
                    } else {
                        // if cantidad equals 0, save item and break
                        item = scItem;
                        break;
                    }
                }
            }

            if (item != null) {
                // remove from carro
                items.remove(item);
            }
        }
    }

    /**
     * Returns the list of <code>ShoppingCarroItems</code>.
     *
     * @return the <code>items</code> list
     * @see CarroItem
     */
    public synchronized List<CarroItem> getItems() {

        return items;
    }

    /**
     * Returns the sum of quantities for all items maintained in shopping carro
     * <code>items</code> list.
     *
     * @return the number of items in shopping carro
     * @see CarroItem
     */
    public synchronized int getNumeroDeItems() {

        numeroDeItems = 0;

        for (CarroItem scItem : items) {

            numeroDeItems += scItem.getCantidad();
        }

        return numeroDeItems;
    }

    /**
     * Returns the sum of the mueble price multiplied by the cantidad for all
     * items in shopping carro list. This is the total cost excluding the surcharge.
     *
     * @return the cost of all items times their quantities
     * @see CarroItem
     */
    public synchronized double getSubtotal() {

        double subtotal = 0;

        for (CarroItem scItem : items) {

            Mueble mueble = (Mueble) scItem.getMueble();
            subtotal += (scItem.getCantidad() * mueble.getPrecio().doubleValue());
        }

        return subtotal;
    }

    /**
     * Calculates the total cost of the orden. This method adds the subtotal to
     * the designated surcharge and sets the <code>total</code> instance variable
     * with the result.
     *
     * @param surcharge the designated surcharge for all ordens
     * @see CarroItem
     */
    public synchronized void calculateTotal(String surcharge) {

        double temp = 0;

        // cast surcharge as double
        double s = Double.parseDouble(surcharge);

        temp = this.getSubtotal();
        temp += s;

        total = temp;
    }

    /**
     * Returns the total cost of the orden for the given
     * <code>Carro</code> instance.
     *
     * @return the cost of all items times their quantities plus surcharge
     */
    public synchronized double getTotal() {

        return total;
    }

    /**
     * Empties the shopping carro. All items are removed from the shopping carro
     * <code>items</code> list, <code>numeroDeItems</code> and
     * <code>total</code> are reset to '<code>0</code>'.
     *
     * @see CarroItem
     */
    public synchronized void clear() {
        items.clear();
        numeroDeItems = 0;
        total = 0;
    }
    
}
