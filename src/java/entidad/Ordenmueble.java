/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ARMAND
 */
@Entity
@Table(name = "ordenmueble")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ordenmueble.findAll", query = "SELECT o FROM Ordenmueble o"),
    @NamedQuery(name = "Ordenmueble.findByIdOrden", query = "SELECT o FROM Ordenmueble o WHERE o.ordenmueblePK.idOrden = :idOrden"),
    })
public class Ordenmueble implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OrdenmueblePK ordenmueblePK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad")
    private short cantidad;
    @JoinColumn(name = "idOrden", referencedColumnName = "idOrden", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Orden orden;
    @JoinColumn(name = "idMueble", referencedColumnName = "idMueble", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Mueble mueble;

    public Ordenmueble() {
    }

    public Ordenmueble(OrdenmueblePK ordenmueblePK) {
        this.ordenmueblePK = ordenmueblePK;
    }

    public Ordenmueble(OrdenmueblePK ordenmueblePK, short cantidad) {
        this.ordenmueblePK = ordenmueblePK;
        this.cantidad = cantidad;
    }

    public Ordenmueble(int idOrden, int idMueble) {
        this.ordenmueblePK = new OrdenmueblePK(idOrden, idMueble);
    }

    public OrdenmueblePK getOrdenmueblePK() {
        return ordenmueblePK;
    }

    public void setOrdenmueblePK(OrdenmueblePK ordenmueblePK) {
        this.ordenmueblePK = ordenmueblePK;
    }

    public short getCantidad() {
        return cantidad;
    }

    public void setCantidad(short cantidad) {
        this.cantidad = cantidad;
    }

    public Orden getOrden() {
        return orden;
    }

    public void setOrden(Orden orden) {
        this.orden = orden;
    }

    public Mueble getMueble() {
        return mueble;
    }

    public void setMueble(Mueble mueble) {
        this.mueble = mueble;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ordenmueblePK != null ? ordenmueblePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ordenmueble)) {
            return false;
        }
        Ordenmueble other = (Ordenmueble) object;
        if ((this.ordenmueblePK == null && other.ordenmueblePK != null) || (this.ordenmueblePK != null && !this.ordenmueblePK.equals(other.ordenmueblePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Ordenmueble[ ordenmueblePK=" + ordenmueblePK + " ]";
    }
    
}
