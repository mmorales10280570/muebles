/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ARMAND
 */
@Embeddable
public class OrdenmueblePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "idOrden")
    private int idOrden;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idMueble")
    private int idMueble;

    public OrdenmueblePK() {
    }

    public OrdenmueblePK(int idOrden, int idMueble) {
        this.idOrden = idOrden;
        this.idMueble = idMueble;
    }

    public int getIdOrden() {
        return idOrden;
    }

    public void setIdOrden(int idOrden) {
        this.idOrden = idOrden;
    }

    public int getIdMueble() {
        return idMueble;
    }

    public void setIdMueble(int idMueble) {
        this.idMueble = idMueble;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idOrden;
        hash += (int) idMueble;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdenmueblePK)) {
            return false;
        }
        OrdenmueblePK other = (OrdenmueblePK) object;
        if (this.idOrden != other.idOrden) {
            return false;
        }
        if (this.idMueble != other.idMueble) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.OrdenmueblePK[ idOrden=" + idOrden + ", idMueble=" + idMueble + " ]";
    }
    
}
