/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import entidad.Mueble;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author ARMAND
 */
@Stateless
public class MuebleFacade extends AbstractFacade<Mueble> {
    @PersistenceContext(unitName = "MuebleriaFPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MuebleFacade() {
        super(Mueble.class);
    }
    
}
