/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validacion;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author ARMAND
 */
public class Validaciones 
{
    public boolean validaCantidad (String idMueble, String cantidad)
    {

        boolean error = false;

        if (!idMueble.isEmpty() && !cantidad.isEmpty()) 
        {
            int i = -1;
            try 
            {
                i = Integer.parseInt(cantidad);
            } 
            catch (Exception e) 
            {
                System.out.println("ERROR EN EL FORMATO DE LA CANTIDAD"+ e.getMessage()); 
            }
            if (i < 0 || i > 200) 
            {

                error = true;
            }
        }
        return error;
    }
    
    public boolean validaDatos(String nombre, String email,String tel,String dir,String cp,String tn,HttpServletRequest request) 
    {
        boolean err = false;

        if (nombre == null|| nombre.equals("")|| nombre.length() > 40) 
        {
            err = true;
            request.setAttribute("errorNombre", err);
        }
        if (email == null|| email.equals("")|| !email.contains("@"))
        {
            err = true;
            request.setAttribute("errorEmail", err);
        }
        if (tel == null|| tel.equals("")|| tel.length()!=9 )
        {
            err = true;
            request.setAttribute("errorTelefono", err);
        }
        if (dir == null|| dir.equals("")|| dir.length() > 60) 
        {
            err = true;
            request.setAttribute("errorDireccion", err);
        }
        if (cp == null|| cp.equals("") || cp.length()!= 2) 
        {
            err = true;
            request.setAttribute("errorCP", err);
        }
        if (tn == null|| tn.equals("")|| tn.length() > 19) 
        {
            err = true;
            request.setAttribute("errorTarjeta", err);
        }

        return err;
    }
    
    
}
