<%-- 
    Document   : index
    Created on : 2/12/2014, 11:20:59 PM
    Author     : ARMAND
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Index muebleria</title>
        <LINK REL=StyleSheet href="css/estilo.css" TYPE="text/css">
    </head>
    <body>
        <div  align="center">
            <table   width="300px">
                <th>
                        <p >
                            Bienvenido a El mueble Chiflado!, Contamos con los mejores muebles, la mejor calidad, y los muebles mas vendidos de 
                            del mercado
                        </p>
                        <img  src="img/logo.jpg">
                        </th>
                        </table>
                    </div>
        <div align="center">
            <table >
                <tr>
                    <td>
                        <p>Ruta para las imágenes de Categorías: ${initParam.categoriasImg}</p>
                    </td>
                    <td>
                       <p>Ruta para las imágenes de Muebles ${initParam.mueblesImg}</p> 
                    </td>
                </tr>
            </table>
        
        
        </div>
                 
               
                    
                        <c:forEach var="categoria" items="${categorias}">
                              <table id="ta" align="center">
                        <th>Categoria</th>
                        <th>Imagen</th>
                        <tr>
                            <td>
                                <span  >${categoria.nombre}</span>
                                
                            </td>
                            <td>
                                <div>
                            <a href="<c:url value='categoria?${categoria.idCategoria}'/>"> 
                                    <img  src="${initParam.categoriasImg}${categoria.nombreImg}.jpg"
                                          alt="${categoria.nombreImg}">
                                    </div> 
                            </td>
                        </tr>
                        
                    </table>    
                                
                        </c:forEach>
                
               
        
    </body>
</html>

